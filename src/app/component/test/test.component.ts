import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  @Input() image: string = '';
  @Input() name: string = '';
  @Input() online: boolean = false;
  @Input() size: string = '';
  @Input() userName: string = '';
  @Input() style: string = 'rounded';
  constructor() {

  }
  
  ngOnInit() {
  }
  
  getInitials(name: string): string {
    const nameParts = this.userName.split(' ');
    return nameParts.map(namePart => namePart.charAt(0).toUpperCase()).join(' ');
  }
}
function getInitials(name: void, string: any) {
  throw new Error('Function not implemented.');
}

